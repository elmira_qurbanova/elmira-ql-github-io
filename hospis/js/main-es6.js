let burgerBtn = document.querySelector('.burger');
let navMenu = document.querySelector('.header__nav');
let menuLinks = document.querySelectorAll('.nav__link');
let btnLink = document.querySelector('.header__button');






burgerBtn.addEventListener('click', function () {
    if(navMenu.classList.contains('open')){
        navMenu.classList.remove('open');
        navMenu.removeAttribute('style');
    }else{
        navMenu.classList.add('open');
        navMenu.style.maxHeight = navMenu.scrollHeight + 'px';
    }
    burgerBtn.classList.toggle('close');
});






window.addEventListener("resize", function(){
    if(window.innerWidth <= 992){
        for(let i = 0; i < menuLinks.length; i++){
    
            menuLinks[i].addEventListener('click', function(){ 
                navMenu.classList.remove('open');
                navMenu.removeAttribute('style');
                burgerBtn.classList.remove('close');
            });
           
            
    
        };
        btnLink.addEventListener('click', function(){
            navMenu.classList.remove('open');
            navMenu.removeAttribute('style');
            burgerBtn.classList.remove('close');
        });
    }
});





        
        (function () {
            if(window.innerWidth > 992){
                const header = document.querySelector('.header');
                const contact = document.querySelector('.contacts');
               
                window.onscroll = () => {
                    
                    if (window.pageYOffset > 300) {
                        
                       header.style.backgroundColor = 'white';
                       header.style.boxShadow = '0px 2px 4px -1px rgba(138,138,138,0.4)'
                        contact.classList.add('contacts__active');
                    } else {
                        header.style.backgroundColor = 'rgba(255, 255, 255, 0.4)';
                        contact.classList.remove('contacts__active');
                    }
                };
            }else{
                
            }
            
        
        }()); 
    

        let form = document.querySelector('.form');
        let validateButton = form.querySelector('.form__button');
        let firstName = form.querySelector('.firstName__field');
        let lastName = form.querySelector('.lasttName__field');
        let service = form.querySelector('.services__field')
        let phone = form.querySelector('.phone__field');
        let date = form.querySelector('.date__field');
        let time = form.querySelector('.time__field');
        let message = form.querySelector('.message__field');
        let fields = form.querySelectorAll('.form-control');

        let generateError = function(text){
            let error = document.createElement('div');
                   error.className ='error';
                   error.style.color = 'red';
                   error.style.fontSize = 12+ 'px';
                   error.style.marginLeft = 20 + 'px'
                   error.innerHTML = text;
                   return error
        }

        form.addEventListener('submit', function(event){
            event.preventDefault();
            let errors = form.querySelectorAll('.error');
            for(let i = 0; i < errors.length; i++){
                errors[i].remove()
            }
            for(let i = 0; i < fields.length; i++){
               if( !fields[i].value){
               let error = generateError('Cannot be blank');
              
                fields[i].parentElement.appendChild(error, fields[i]);
               }
            }


        })



      
           
        const counters = document.querySelectorAll('.amount');
        const speed = 200; // The lower the slower

        counters.forEach(counter => {
        const updateCount = () => {
        const target = +counter.getAttribute('data-target');
        const count = +counter.innerText;

        // Lower inc to slow and higher to slow
        const inc = target / speed;

        // console.log(inc);
        // console.log(count);

        // Check if target is reached
        if (count < target) {
            // Add inc to count and output in counter
            counter.innerText = Math.ceil(count + inc);
            // Call function every ms
            setTimeout(updateCount, 1);
        } else {
            counter.innerText = target;
        }
};

        updateCount();
});




$(document).ready(function(){
    $(".owl-carousel").owlCarousel({items:3, loop:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

  });




      function initMap (){
      let location = {lat:40.712776, lng: -74.005974};
      let map = new google.maps.Map(document.getElementById('map'),{zoom:4,
        center:location});
  }
 







  let contactForm = document.querySelector('.contact-us__form');
  let contactButton = document.querySelector('.contact-us-button');
  let contactForms = document.querySelectorAll('.contact__form_control');



    contactForm.addEventListener('submit', function(event){
    event.preventDefault();
    let errors = contactForm.querySelectorAll('.error');

    for(let i = 0; i < errors.length; i++){
        errors[i].remove()
    }

    for(let i = 0; i < contactForms.length; i++ ){
        if(!contactForms[i].value){
           let error = generateError('Cannot be blank');
           contactForms[i].parentElement.appendChild(error, contactForms[i]);

        }
    }
});

// Scroll to anchors
(function () {

    const smoothScroll = function (targetEl, duration) {
        const headerElHeight =  document.querySelector('.header__main').clientHeight;
        let target = document.querySelector(targetEl);
        let targetPosition = target.getBoundingClientRect().top - headerElHeight;
        let startPosition = window.pageYOffset;
        let startTime = null;
    
        const ease = function(t,b,c,d) {
            t /= d / 2;
            if (t < 1) return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };
    
        const animation = function(currentTime){
            if (startTime === null) startTime = currentTime;
            const timeElapsed = currentTime - startTime;
            const run = ease(timeElapsed, startPosition, targetPosition, duration);
            window.scrollTo(0,run);
            if (timeElapsed < duration) requestAnimationFrame(animation);
        };
        requestAnimationFrame(animation);

    };

    const scrollTo = function () {
        const links = document.querySelectorAll('.js-scroll');
        links.forEach(each => {
            each.addEventListener('click', function () {
                const currentTarget = this.getAttribute('href');
                smoothScroll(currentTarget, 1000);
            });
        });
    };
    scrollTo();
}());



 