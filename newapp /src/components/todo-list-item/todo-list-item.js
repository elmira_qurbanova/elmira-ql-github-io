import React, {Component} from 'react';

import './todo-list-item.css';

export default class  TodoListItem extends Component   {
  
    
   render(){
    const {label, onDeleted, onToggleDone, onToggleImportant, important, done}=this.props; // теперь внутри функции у нас появится переменные с названием label , important

    let classNames = 'todo-list-item d-flex align-items-center';
    if(done){classNames+=' done'};
    if(important){classNames+=' important'}
    return (
        <span className={classNames}>
              <span  className='todo-list-item-label flex-grow-1' onClick={onToggleDone}>
             {label} 
             </span>
             <button className='btn btn-outline-danger'
                        onClick={onDeleted}><i className="fa fa-trash" aria-hidden="true"></i></button>
             <button onClick={onToggleImportant}
              className='btn btn-outline-success'><i className="fa fa-exclamation" aria-hidden="true"></i></button>
        </span>
      
    )
   }
   
}

 