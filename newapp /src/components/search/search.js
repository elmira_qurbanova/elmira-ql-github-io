import React, {Component} from 'react';

import ItemStatusFilter from '../item-status-filter/item-status-filter';
 
import './search.css';


export default class Search extends Component {
  state={
    term:''
  }
  onSearchChange=(e)=>{
    const term = e.target.value;// получаем текущее значение инпута
    this.setState({term});// сохраняем его в новое  состояние - из пустого в вводимое 
    this.props.onSearchChange(term)// отправляем введенное на уровень выше и меняем там состояние
  }
  render(){
    const {filter,onFilterChange}=this.props;
    return (
      <div className='search-wrapper  d-flex flex-fill'>
         <input placeholder='search'
                className='search'
                value={this.state.term}
                onChange={this.onSearchChange}
                />
         <ItemStatusFilter filter={this.props.filter} 
                           onFilterChange={this.props.onFilterChange}
                            />
      </div>
     
    )
  }
    
  };

  