import React, {Component} from 'react';
import TodoHeader from '../header/header';
import Search from '../search/search';
import TodoList from '../todo-list/todo-list';
import AddItem from '../add-item/add-item';

import './app.css'

export default class  App extends Component {
    maxId=10;
    state={
        todoData : [
           this.createTodoItem('10 minuties meditation'),
           this.createTodoItem('Exercise'),
           this.createTodoItem('Have a breakfast'),
           this.createTodoItem("Doctor's appointment"), 
           this.createTodoItem('Call mom'),
           this.createTodoItem('Work'), 
           this.createTodoItem('Learn lessons'),
           this.createTodoItem('Walk a dog'),
           this.createTodoItem('Good night'), 
          ],
          term:'',
          filter:'all'// all, active, done
    }
    
createTodoItem(label){
  return{
    label:label,
    important: false,
    done:false,
    id: this.maxId++
  }
}

    deleteItem=(id)=>{
        this.setState(({todoData})=>{
        const idx = todoData.findIndex((el)=> el.id===id);//индекс элемента который мы хотим удалять
         //const before = todoData.slice(0, idx);//кусочек массива. тоже массив
        //const after = todoData.slice(idx+1);//кусочек массива. тоже массив 
        const newArr =  [...todoData.slice(0, idx), ...todoData.slice(idx+1)];

        return{
            todoData: newArr
        }
        })
    }

    addItem = (text) => {
       const newItem = this.createTodoItem(text)
       this.setState(({todoData})=>{
      
        const newArray = [...todoData,newItem ];
        return{
            todoData:newArray
        }
       })
    }


toggleProperty(arr, id, propName){
   
        const idx = arr.findIndex((el)=> el.id===id);
        const oldItem = arr[idx];
        const newItem = {...oldItem, [propName]:!oldItem[propName]};
        return   [...arr.slice(0, idx), newItem , ...arr.slice(idx+1)];

        

       
}


    onToggleDone=(id)=>{
    
      this.setState(({todoData})=>{
          return{
              todoData : this.toggleProperty(todoData, id, 'done')
          }
      })
    }
    onToggleImportant=(id)=>{
        this.setState(({todoData})=>{
            return{
                todoData : this.toggleProperty(todoData, id, 'important')
            }
        })
    }

search(items, term){
    if(term===0){
        return items;
    }
  
       return items.filter((item)=>{
            return item.label.toLowerCase().indexOf(term.toLowerCase())>-1;
    
  
    })

}


filter(items, filter){
    switch (filter){
        case 'all': return items;
        case 'active': return items.filter((item)=> !item.done);
        case 'done': return items.filter((item)=> item.done);
        default: return items;
    }
}


onSearchChange=(term)=>{
this.setState({term})
}

onFilterChange=(filter)=>{
    this.setState({filter})
}

  render(){
    const {todoData, term, filter}=this.state;
    const visibleItems = this.filter(this.search(todoData, term), filter);
    const doneCounter  = todoData.filter((el)=>el.done).length;
    const todoCounter = todoData.length-doneCounter;
    return(
        <div className='app-wrapper col-12 col-md-5 mx-auto'>
          <TodoHeader todo={todoCounter} done={doneCounter}/>
          <Search   onSearchChange={this.onSearchChange}
                    filter={this.filter(filter)}
                    onFilterChange={this.onFilterChange}
                    />
          <TodoList todos={visibleItems}
                    onDeleted={this.deleteItem} 
                    onToggleDone={this.onToggleDone}
                    onToggleImportant={this.onToggleImportant}
                   /> 
          <AddItem  onItemAdded={this.addItem} />
        </div>
      )
  }
   
  };

