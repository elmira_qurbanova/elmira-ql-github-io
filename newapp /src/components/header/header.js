import React from 'react';

import './header.css';



const TodoHeader =({todo, done})=>{
    return (
      <div className='header-wrapper'>
        <h1>To-do list</h1>
        <h6 className='text-secondary'>{todo} more to do, {done} done</h6>
      </div>
  
    )
  };
  
  export default TodoHeader;